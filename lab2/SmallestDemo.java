
public class SmallestDemo {

    public static void main(String[] args){

    	int value1 = Integer.parseInt(args[0]);				//yukarda args normalde string. biz bu kodla integera çevirdik
        int value2 = Integer.parseInt(args[1]);
		int value3 = Integer.parseInt(args[2]);
        int result;

        boolean someCondition = value1 < value2;

        result = someCondition ? value1 : value2;  //üst satırda boolean doğru ya da yanlış olduğunu belirtir
													// doğruysa result=value1  yanlışsa result = value2 
		someCondition=result < value3;
		
		result=someCondition ? result : value3;

        System.out.println(result);	
    }
}
