package iostreams;

import java.text.*;
import java.io.*;
 
/**
 * StudentRecordReader.java
 * This program illustrates how to use the ObjectInputStream class for reading
 * objects from a file.
 *
 * @author www.codejava.net
 */
public class StudentRecordReader {
 
    public static void main(String[] args) {
 
        String inputFile = "week14/students.rec";
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
 
        try (
                ObjectInputStream objectInput
                    = new ObjectInputStream(new FileInputStream(inputFile));
            ){
 
            while (true) {
                Student student = (Student) objectInput.readObject();
 
                System.out.print(student.getName() + "\t");
                System.out.print(dateFormat.format(student.getBirthday()) + "\t");
                System.out.print(student.getGender() + "\t");
                System.out.print(student.getAge() + "\t");
                System.out.println(student.getGrade());
            }
 
        } catch (EOFException eof) {
            System.out.println("Reached end of file");
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
 
}